FROM python:3.9-slim as builder

# hadolint ignore=DL3008
RUN apt-get --quiet --quiet update \
    && apt-get --yes --quiet --quiet install --no-install-recommends \
        build-essential \
        gcc \
        git

ENV PATH="/opt/venv/bin:$PATH"
# Some vsketch dependency will downgrade setuptools to 51.3.3 anyway
ENV PYTHON_SETUPTOOLS_VERSION=51.3.3
# hadolint ignore=DL3013
RUN python -m venv /opt/venv \
    && \
    pip --disable-pip-version-check --no-cache-dir install --upgrade \
        pip \
        setuptools==${PYTHON_SETUPTOOLS_VERSION} \
        wheel \
    && \
    pip --disable-pip-version-check --no-cache-dir install \
        git+https://github.com/abey79/vsketch#egg=vsketch \
        prettymaps \
    && \
    echo && du -sh /opt/venv/ && echo


FROM python:3.9-slim as final

COPY --from=builder /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# https://github.com/marceloprates/prettymaps/blob/main/prettymaps/draw.py#L321
# hadolint ignore=DL3008
RUN echo 'deb http://deb.debian.org/debian bullseye non-free' > /etc/apt/sources.list.d/nonfree.list \
    && apt-get --quiet --quiet update \
    && apt-get --yes --quiet --quiet install --no-install-recommends \
        fonts-ubuntu \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /src

#RUN useradd --create-home --system user \
#    && chown user /src
#USER user

CMD [ "/bin/bash" ]
